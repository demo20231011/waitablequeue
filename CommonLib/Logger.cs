﻿using System;
using System.Diagnostics;

namespace CommonLib
{
	public static class Logger
	{
		private static object _Lock = new object();

		public static void Log(ConsoleColor color, string text = null)
		{
			lock (_Lock)
			{
				Debug.WriteLine(text);

				var savedColor = Console.ForegroundColor;
				Console.ForegroundColor = color;

				Console.WriteLine(text);

				Console.ForegroundColor = savedColor;
			}
		}
	}
}
