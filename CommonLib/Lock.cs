﻿using System;
using System.Threading;

namespace CommonLib
{
	public class Lock
	{
		public T ChangeAndNotifyOne<T>(Func<T> changeAndGet)
		{
			lock (this)
			{
				var result = changeAndGet();

				Monitor.Pulse(this);

				return result;
			}
		}
		public void Wait(Func<bool> condition)
		{
			lock (this)
			{
				while (!condition())
				{
					Monitor.Wait(this);
				}
			}
		}
		public T WaitAndGet<T>(Func<bool> condition, Func<T> getter)
		{
			lock (this)
			{
				while (!condition())
				{
					Monitor.Wait(this);
				}

				return getter();
			}
		}
		public T Get<T>(Func<T> getter)
		{
			lock (this) return getter();
		}
	}
}
