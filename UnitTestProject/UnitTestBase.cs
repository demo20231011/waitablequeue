﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WaitableQueueLib;

namespace UnitTestProject
{
	public class UnitTestBase<T> where T : IWaitableQueue<int>, new()
	{
		protected IWaitableQueue<int> CreateQueue() => new T();

		[TestMethod]
		public void SingleThreaded()
		{
			var q = CreateQueue();

			for (int i = 0; i < 10; i++)
			{
				q.push(i);
			}

			for (int i = 0; i < 10; i++)
			{
				var v = q.pop();

				Assert.AreEqual(i, v);
			}
		}
		[TestMethod]
		public void PopMustWait()
		{
			var q = CreateQueue();

			var taskRecievers = Enumerable.Range(0, 3)
				.Select(i => Task.Run(() =>
				{
					return q.pop();
				}))
				.ToArray();

			Thread.Sleep(100);

			// assert: no task stopped
			Assert.IsFalse(taskRecievers.Any(t => t.IsFaulted), GetErrorMessages(taskRecievers));
			Assert.IsFalse(taskRecievers.Any(t => t.IsCompleted));

			q.push(123);

			Thread.Sleep(100);

			// assert: no task faulted
			Assert.IsFalse(taskRecievers.Any(t => t.IsFaulted), GetErrorMessages(taskRecievers));

			// assert: only one task popped a value
			Assert.AreEqual(1, taskRecievers.Where(t => t.IsCompleted).Count());

			// assert: the value popped is correct
			Assert.AreEqual(123, taskRecievers.Where(t => t.IsCompleted).First().Result);
		}
		[TestMethod]
		public void MultipleReceivers()
		{
			var q = CreateQueue();

			var results = new ConcurrentBag<int>();

			var taskRecievers = Enumerable.Range(0, 3)
				.Select(i => Task.Run(() =>
				{
					while (true)
					{
						var v = q.pop();
						results.Add(v);

						//to avoid memory overflow if something goes wrong
						if (results.Count > 10000000) throw new Exception("too many results");
					}
				}))
				.ToArray();

			Thread.Sleep(100);

			// assert: no task stopped
			Assert.IsFalse(taskRecievers.Any(t => t.IsFaulted), GetErrorMessages(taskRecievers));
			Assert.IsFalse(taskRecievers.Any(t => t.IsCompleted));

			// assert: pop must wait
			Assert.IsTrue(results.IsEmpty, $"results.Count={results.Count}");

			for (int i = 0; i < 10; i++)
			{
				q.push(i);

				Thread.Sleep(100);

				// assert: no task stopped
				Assert.IsFalse(taskRecievers.Any(t => t.IsFaulted), GetErrorMessages(taskRecievers));
				Assert.IsFalse(taskRecievers.Any(t => t.IsCompleted));

				// only one receiver popped a value
				Assert.AreEqual(1, results.Count);

				Assert.IsTrue(results.TryTake(out var item));

				// assert: the value popped is correct
				Assert.AreEqual(i, item);
			}

			// unfortunatelly, we can not stop taskReciever tasks until the process get exited, since pop is not cancellable
		}

		private static string GetErrorMessages(Task[] taskRecievers)
		{
			return taskRecievers
				.Where(t => t.IsFaulted)
				.Select(t => $"task#{t.Id}: {t.Exception.GetMessages()}")
				.ToStringSeparated("\n");
		}
	}
}
