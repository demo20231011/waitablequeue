﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProject
{
	static class Utils
	{
		public static string GetMessages(this Exception x)
		{
			return x.GetInnerExceptions()
				.Where(x2 => !(x2 is AggregateException))
				.Select(x2 => x2.Message)
				.Reverse()
				.ToStringSeparated("\n");
		}
		public static IEnumerable<Exception> GetInnerExceptions(this Exception x)
		{
			while (x != null)
			{
				yield return x;

				x = x.InnerException;
			}
		}
		public static string ToStringSeparated<T>(this IEnumerable<T> values, string separator)
		{
			return string.Join(separator, values);
		}
	}
}
