﻿using System;
using System.Diagnostics;
using CommonLib;
using WaitableQueueLib;

namespace WaitableQueueTestApp
{
	class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				ShowLogo();

				ShowChooseClassToTest();

				var q = ChooseClassToTest();
				if (q != null)
				{
					Run(q);
				}
			}
			catch (Exception x)
			{
				Logger.Log(ConsoleColor.Red, x.Message);

				if (Debugger.IsAttached)
				{
					Logger.Log(ConsoleColor.Gray, "Press any key to exit");
					Console.ReadKey(true);
				}
			}
		}
		private static void ShowChooseClassToTest()
		{
			Logger.Log(ConsoleColor.Green, "Now press");
			Logger.Log(ConsoleColor.Green, "1 - to test WaitableQueue1 (using Monitor Pulse/Wait)");
			Logger.Log(ConsoleColor.Green, "2 - to test WaitableQueue2 (using AutoResetEvent)");
			Logger.Log(ConsoleColor.Green, "ESC - to quit.");
		}
		private static IWaitableQueue<Message> ChooseClassToTest()
		{
			while (true)
			{
				var cki = Console.ReadKey(true);
				switch (cki.Key)
				{
					case ConsoleKey.D1: return new WaitableQueue1<Message>();
					case ConsoleKey.D2: return new WaitableQueue2<Message>();
					case ConsoleKey.Escape: return null;
				}
			}
		}
		private static void ShowLogo()
		{
			Logger.Log(ConsoleColor.Yellow, "WaitableQueue testing app");
			Logger.Log(ConsoleColor.Yellow);
		}
		private static void ShowKeyboardControl()
		{
			Logger.Log(ConsoleColor.White);
			Logger.Log(ConsoleColor.White, "Keyboard control:");
			Logger.Log(ConsoleColor.White);
			Logger.Log(ConsoleColor.White, "1 - start/stop Sender 1");
			Logger.Log(ConsoleColor.White, "2 - start/stop Sender 2");
			Logger.Log(ConsoleColor.White, "Q - send message by Sender 1");
			Logger.Log(ConsoleColor.White, "W - send message by Sender 2");
			Logger.Log(ConsoleColor.White, "R - start/stop Receiver 1");
			Logger.Log(ConsoleColor.White, "T - start/stop Receiver 2");
			Logger.Log(ConsoleColor.White, "ESC - exit");
			Logger.Log(ConsoleColor.White);
		}
		private static void Run(IWaitableQueue<Message> q)
		{
			ShowKeyboardControl();

			var reciever1 = new Receiver(paused: true, queue: q, name: "Receiver1", delay: TimeSpan.FromSeconds(2), color: ConsoleColor.Magenta);
			reciever1.Start();

			var reciever2 = new Receiver(paused: true, queue: q, name: "Receiver2", delay: TimeSpan.FromSeconds(5), color: ConsoleColor.Blue);
			reciever2.Start();

			var sender1 = new Sender(paused: true, queue: q, name: "Sender 1", delay: TimeSpan.FromSeconds(1), color: ConsoleColor.Green);
			sender1.Start();

			var sender2 = new Sender(paused: true, queue: q, name: "Sender 2", delay: TimeSpan.FromSeconds(2.3), color: ConsoleColor.Yellow);
			sender2.Start();

			while (true)
			{
				var cki = Console.ReadKey(true);
				switch (cki.Key)
				{
					case ConsoleKey.D1: sender1.TogglePaused(); break;
					case ConsoleKey.D2: sender2.TogglePaused(); break;
					case ConsoleKey.Q: sender1.Send(); break;
					case ConsoleKey.W: sender2.Send(); break;
					case ConsoleKey.R: reciever1.TogglePaused(); break;
					case ConsoleKey.T: reciever2.TogglePaused(); break;
					case ConsoleKey.Escape: return;
					default: ShowKeyboardControl(); break;
				}
			}
		}
	}

	class Message
	{
		public string senderName;
		public string info;

		public override string ToString()
		{
			return $"Message(senderName=[{senderName}], info=[{info}])";
		}
	}
}
