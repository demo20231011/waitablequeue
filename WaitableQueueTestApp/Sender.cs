﻿using System;
using System.Threading;
using CommonLib;
using WaitableQueueLib;

namespace WaitableQueueTestApp
{
	class Sender : TaskRunner
	{
		private IWaitableQueue<Message> _q;
		private TimeSpan _delay;
		private int _MsgIndex;
		private Lock _MsgIndexLock = new Lock();

		public Sender(bool paused, IWaitableQueue<Message> queue, string name, TimeSpan delay, ConsoleColor color) : base(paused, name, color)
		{
			_q = queue;
			_delay = delay;
		}

		protected override void Run()
		{
			Thread.Sleep(_delay); // simulate work

			if (!IsPaused) Send();
		}

		public void Send()
		{
			var msgIndex = _MsgIndexLock.Get(() => ++_MsgIndex);

			var item = new Message
			{
				senderName = _name,
				info = $"msg #{msgIndex} from {_name}",
			};

			Log($"push message: info={item}");

			_q.push(item);
		}
	}
}
