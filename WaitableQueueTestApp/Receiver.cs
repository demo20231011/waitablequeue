﻿using System;
using System.Threading;
using WaitableQueueLib;

namespace WaitableQueueTestApp
{
	class Receiver : TaskRunner
	{
		private IWaitableQueue<Message> _q;
		private TimeSpan _delay;

		public Receiver(bool paused, IWaitableQueue<Message> queue, string name, TimeSpan delay, ConsoleColor color) : base(paused, name, color)
		{
			_q = queue;
			_delay = delay;
		}

		protected override void Run()
		{
			var msg = _q.pop();

			Log($"popped message: info={msg}");

			Thread.Sleep(_delay); // simulate msg processing

			Log($"processed message: info={msg}");

			if (IsPaused) return;
		}
	}
}
