﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CommonLib;

namespace WaitableQueueTestApp
{
	abstract class TaskRunner
	{
		private ConsoleColor _color;
		protected string _name;

		private bool _isPaused;
		private Lock _isPausedLock = new Lock();

		public TaskRunner(bool paused, string name, ConsoleColor color)
		{
			_name = name;
			_color = color;
			_isPaused = paused;
		}
		public void Start()
		{
			Task.Run(() =>
			{
				try
				{
					var isPaused = _isPausedLock.Get(() => _isPaused);
					Log(isPaused ? "PAUSED" : "RUNNING");

					while (true) // we provide no loop exit condition since the thread will be aborted on the program end
					{
						if (IsPaused)
						{
							_isPausedLock.Wait(() => !_isPaused);
						}
						else
						{
							Run();
						}
					}
				}
				catch (Exception x)
				{
					Log("ERROR: " + x.Message);
					if (Debugger.IsAttached) Debugger.Break();
				}
			});
		}

		public void TogglePaused()
		{
			var isPaused = _isPausedLock.ChangeAndNotifyOne(() =>
			{
				_isPaused = !_isPaused;

				return _isPaused;
			});

			Log(isPaused ? "PAUSED" : "RUNNING");
		}

		protected bool IsPaused
		{
			get { lock (_isPausedLock) return _isPaused; }
		}

		protected void Log(string text)
		{
			Logger.Log(_color, $"{_name}: {text}");
		}

		protected abstract void Run();
	}
}
