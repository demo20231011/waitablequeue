﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CommonLib;

namespace WaitableQueueLib
{
	public class WaitableQueue1<T> : IWaitableQueue<T>
	{
		private Queue<T> _queue = new Queue<T>();
		private Lock _lock = new Lock();

		public void push(T item)
		{
			// we don't check for the queue overflow here, since it is not required by the Task Requirements,
			// but in the production code we should define a limit and wait until the queue is not full or ignore incoming items
			// or delete some messages with low priority from the queue when the item count reaches the limit.

			// since we added only one item, we can 'notify one' to awake one processing thread only

			var count = _lock.ChangeAndNotifyOne(() =>
			{
				_queue.Enqueue(item);

				return _queue.Count; // in the production we need not to return anything here
			});

			Log($"pushed: {item}; queue size: {count};");
		}
		public T pop()
		{
			// in the production code we should add a way to cancel awaiting

			// wait until _queue is not empty and dequeue an item

			var r = _lock.WaitAndGet(
				() => _queue.Count > 0,
				() =>
				{
					var item = _queue.Dequeue();

					// since we have not added items to the queue, there is no reason to notify other waiting threads.

					return (item: item, count: _queue.Count); // we use tuple here to get _queue.Count; in the production we have to return just the item.
				});

			Log($"popped: {r.item}; queue size: {r.count};");

			return r.item;
		}

		[Conditional("DEBUG")]
		private void Log(string text)
		{
			Logger.Log(ConsoleColor.DarkGray, $"WaitableQueue1: {text}");
		}
	}
}
