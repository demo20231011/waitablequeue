﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using CommonLib;

namespace WaitableQueueLib
{
	public class WaitableQueue2<T> : IWaitableQueue<T>
	{
		private Queue<T> _queue = new Queue<T>();
		private AutoResetEvent _evt = new AutoResetEvent(false);

		public void push(T item)
		{
			// we don't check for the queue overflow here, since it is not required by the Task Requirements,
			// but in the production code we should define a limit and wait until the queue is not full or ignore incoming items
			// or delete some messages with low priority from the queue when the item count reaches the limit.

			int count;

			lock (_queue)
			{
				_queue.Enqueue(item);
				_evt.Set(); // set signaled since we have at least one item in the queue

				count = _queue.Count; // in the production we need not to return count
			}

			Log($"pushed: {item}; queue size: {count};");
		}
		public T pop()
		{
			// in the production code we should add a way to cancel awaiting

			var result = default(T);
			var resultReady = false;
			var count = 0;

			while (!resultReady)
			{
				_evt.WaitOne();  // wait until at least one item is in the queue, then enter and close the door immediately

				// at this point _evt.Set() might be called somewhere, so another thread could come here, so we need to check _queue.Count

				lock (_queue)
				{
					// if another thread has already stolen the item, we have to go to wait again
					if (_queue.Count > 0)
					{
						result = _queue.Dequeue();
						resultReady = true;

						count = _queue.Count;

						if (count > 0) _evt.Set(); // if the queue is still not empty, open the door for the next receiver (or for itself)
					}
				}
			}

			Log($"popped: {result}; queue size: {count};");

			return result;
		}

		[Conditional("DEBUG")]
		private void Log(string text)
		{
			Logger.Log(ConsoleColor.DarkGray, $"WaitableQueue2: {text}");
		}
	}
}
