﻿namespace WaitableQueueLib
{
	public interface IWaitableQueue<T>
	{
		void push(T item);
		T pop();
	}
}